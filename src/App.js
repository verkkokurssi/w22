import logo from './logo.svg';
import './App.css';
import './ComponentName.js';
import ComponentName from './ComponentName.js';

function App() {
  return (
    <div className="App">
      <ComponentName country="Finland" />
    </div>
  );
}

export default App;
